<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Seamos Cultura</title>
    <h1>Mapa Cultural de Argentina<h1>
</head>

<body>

    <?php

    include("db.php");

    $query = "SELECT * FROM provincias";
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_all($result);

    ?>

    <form action="index.php" method="POST">

        <label for="seleccion">Seleccionar una Provincia</label>

        <br>
        <br>

        <select name="selector" id="seleccion">

            <?php

            foreach ($row as $result) :

            ?>

                <option value="<?= $result[0] ?>"><?= $result[1] ?></option> //Valores de la tabla auxiliar

            <?php

            endforeach;

            ?>

        </select>

        <br>
        <br>

        <input type="submit" value="Enviar">

    </form>

    <br>

    <?php

    if ($_POST) :

        $provincia = $_POST['selector'];

        $query = "SELECT * FROM departamento WHERE prov_id = '$provincia'";
        $result = mysqli_query($conn, $query);
        $row = mysqli_fetch_all($result);

    ?>

        <form action="resultado.php" method="POST">

            <label for="seleccion">Seleccionar un departamento</label>

            <br>
            <br>

            <select name="departamento" id="seleccion">

                <?php

                foreach ($row as $result) :

                ?>

                    <option value="<?= $result[0] ?>"><?= $result[2] ?></option> <!-- Valores de la tabla auxiliar -->

                <?php

                endforeach;

                ?>

            </select>
            <input type="hidden" name="provincia" value="<?= $provincia ?>">

            <br>
            <br>

            <input type="submit" value="Enviar">

        </form>

    <?php

    endif;

    ?>
