<?php

const DB_SERVER = 'mysqldb';
const DB_USERNAME = 'root';
const DB_PASSWORD = 'root';
const DB_NAME = 'webcultura_prueba';
 
$conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

$tildes = $conn->query("SET NAMES 'utf8'"); //para que se muestren las tildes
 
if($conn === false){
    die("ERROR: No se ha podido establecer la conexión. " . mysqli_connect_error());
}
?>
